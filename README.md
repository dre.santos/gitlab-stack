### Maintainer: techguru@byiq.com
Copyright (c) 2017,  Cloud Git -- All Rights Reserved

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

# Cloud Belt
A framework and tool set where monolithic full-stack applications can be 
decomposed into highly scalable micro-service architectures deployed on a multi-vendor
cloud platform across the globe.

### Cloud Belt Framework
Supports:
* ingress of transactions via ANYCAST, DNS round-robin, and third party load balancers.
* fault tolerant and resilient
* shared nothing, stateless, versioned logic containers
* global block-storage in consistent hash rings to be fault tolerant and resilient in
the event of outages of:
    * individual cloud providers
    * regions
    * availability zones
    * data centers,
    * racks
    * cpu nodes
    * containers failures,
    * application logic errors.
* multiple versions of the same container at the same time
* circuit breaker pattern
* highly scalable caching and consistent session state.
* chaos monkey at the level of container, node, datacenter, region
* A/B parallel testing comparison of results
* A/B parallel testing of creation of persistent data
* all data versioned -- allows for automated selection of `code that knows`
* queueing and and auto-scale managed by framework
* egress nodes for web-hooks
* egress nodes for web-mail
* egress nodes for 3rd party API
* testing using replay logs at most container boundaries

# Proof of Concept
Cloud Git Cluster is the original proof of concept of Cloud Belt's tools and framework.

# Cloud Git Cluster
A highly scalable and distributed hosting solution for GIT repositories using GitLab CE.
Provides:
1. Certbot/LetsEncrypt automated registration of SSL certificates for public sites
2. Configure small set (11) environment variables to fully deploy a site
3. Site is decomposed as a separate docker service for each server Role used by GitLab

### Public Release August 2017
1. All GitLab containers now `from source`
2. Added Prometheus monitoring container
3. Added Grafana Dashboards
4. Added Google Container Advisor metrics collection
5. Added Docker Registry from official containers.

### Public Release July 2017
1. One-Click deployment after setting small set of configuration variables
2. "A+" rating on https://www.ssllabs.com/ssltest
3. Intended as a development fixture for testing of a decoupled-version of GitLab component services
for behavioral observation and error injection.

# Further opportunities
Other targets include open source projects such as:
* Wikimedia
* Wordpress
* Alfresco

# Use of Proof of Concept
1. read `cloud-git-cluster/README.md`

# License
MIT License terms per the `License` file

