#!/bin/sh -
#
# Maintainer: techguru@byiq.com
#
# Copyright (c) 2017,  Cloud Git -- All Rights Reserved
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

export CLOUD_GIT_REGISTRY_ROOT=/var/opt/gitlab/gitlab-rails/shared/registry
export CLOUD_GIT_REGISTRY_HOME=/var/opt/gitlab/registry

apk add --update curl openssl gettext && rm -rf /var/cache/apk/*

addgroup -g 993 registry
adduser -u 993 -G registry -s /bin/sh -h ${CLOUD_GIT_REGISTRY_HOME} -D registry
mkdir -p ${CLOUD_GIT_REGISTRY_ROOT}
chmod 6750 ${CLOUD_GIT_REGISTRY_ROOT}
chown -R registry:registry ${CLOUD_GIT_REGISTRY_ROOT}
