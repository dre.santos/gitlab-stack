#
# Maintainer: techguru@byiq.com
#
# Copyright (c) 2017,  Cloud Git -- All Rights Reserved
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
CLOUD_GIT_PROJECT=dev \
CLOUD_GIT_VERSION_TAG=0.0.3 \
CLOUD_GIT_DOMAIN=cloud-git.example.com \
CLOUD_GIT_SUBDOMAIN=www \
CLOUD_GIT_ADMIN_EMAIL="admin@cloud-git.example.com" \
CLOUD_GIT_STAGING=staging \
CLOUD_GIT_DOCKER_MACHINE=gitlab001 \
CLOUD_GIT_EMAIL_FROM="cloud-git@cloud-git.example.com" \
CLOUD_GIT_EMAIL_DISPLAY_NAME='Cloud\ Git\ @\ Cloud-Git.Example.com' \
CLOUD_GIT_EMAIL_REPLY_TO="cloud-git@cloud-git.example.com" \
CLOUD_GIT_EMAIL_SUBJECT_SUFFIX="" \
make down
