#!/bin/sh -
#
# Maintainer: techguru@byiq.com
#
# Copyright (c) 2017,  Cloud Git -- All Rights Reserved
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
set -e # fail on errors

export CLOUD_GIT_GITLAB_SHELL_REPO=https://gitlab.com/gitlab-org/gitlab-shell.git
export CLOUD_GIT_GITLAB_SHELL_REPO_DIR=/assets/gitlab-shell
export CLOUD_GIT_SSH_HOST_KEY_DIR=/var/opt/gitlab/gitlab-shell/.host_keys
export CLOUD_GIT_GITLAB_SHELL_REPO_TAG=v5.1.1

apk add --update curl gettext git openssh redis && rm -rf /var/cache/apk/*

addgroup -g 998 git
adduser -u 998 -G git -s /bin/sh -h /var/opt/gitlab -D git
echo git:$(hexdump -n 64 -v -e '1/1 "%02X"' /dev/urandom) | chpasswd

# global git settings
git config --global gc.auto 0
git config --global core.autocrlf input
git config --global repack.writeBitmaps true

mkdir -p ${CLOUD_GIT_GITLAB_SHELL_REPO_DIR}

git -c advice.detachedHead=false clone \
    -b ${CLOUD_GIT_GITLAB_SHELL_REPO_TAG} \
    --depth 1 \
    ${CLOUD_GIT_GITLAB_SHELL_REPO} \
    ${CLOUD_GIT_GITLAB_SHELL_REPO_DIR}

(cd ${CLOUD_GIT_GITLAB_SHELL_REPO_DIR} && RAILS_ENV=production bundle install --jobs 4)

#
# "runas" git user
#
chown git:git /assets/gitlab_ssh_entrypoint.sh
chown -R git:git ${CLOUD_GIT_GITLAB_SHELL_REPO_DIR}
chmod -R g+s,u+s ${CLOUD_GIT_GITLAB_SHELL_REPO_DIR}/bin/*

#
# link binary to location used by gitlab rails authorized_keys until
# we @@ TODO modify the code that generates authorized_keys
#
mkdir -p /opt/gitlab/embedded/service/gitlab-shell/bin
ln -s /assets/gitlab_ssh_entrypoint.sh /opt/gitlab/embedded/service/gitlab-shell/bin/gitlab-shell
ln -s /assets/config.yml ${CLOUD_GIT_GITLAB_SHELL_REPO_DIR}/config.yml

#
# disable gitaly
#
sed --in-place "s/GITALY_MIGRATED_COMMANDS = {/GITALY_MIGRATED_COMMANDS = {} XX = {/" /assets/gitlab-shell/lib/gitlab_shell.rb

#
# set up logging
#
mkdir -p /var/log/gitlab/gitlab-shell
chown -R git:git /var/log/gitlab
chmod -R +w /var/log/gitlab

#
# atomically generate host keys if they are not already defined
#
mkdir -p /var/opt/gitlab/gitlab-shell
if mkdir ${CLOUD_GIT_SSH_HOST_KEY_DIR}
then
	ssh-keygen -f ${CLOUD_GIT_SSH_HOST_KEY_DIR}/ssh_host_rsa_key -N '' -t rsa
	ssh-keygen -f ${CLOUD_GIT_SSH_HOST_KEY_DIR}/ssh_host_ecdsa_key -N '' -t ecdsa
	ssh-keygen -f ${CLOUD_GIT_SSH_HOST_KEY_DIR}/ssh_host_ed25519_key -N '' -t ed25519
fi

exit 0
