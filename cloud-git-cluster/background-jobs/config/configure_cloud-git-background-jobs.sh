#!/bin/sh -
#
# Maintainer: techguru@byiq.com
#
# Copyright (c) 2017,  Cloud Git -- All Rights Reserved
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
set -e # fail on errors

export CLOUD_GIT_GITLAB_REPO=https://gitlab.com/gitlab-org/gitlab-ce.git
export CLOUD_GIT_GITLAB_REPO_DIR=/assets/gitlab-ce
export CLOUD_GIT_GITLAB_REPO_TAG=v9.3.6
export CLOUD_GIT_GITLAB_SHELL_REPO=https://gitlab.com/gitlab-org/gitlab-shell.git
export CLOUD_GIT_GITLAB_SHELL_REPO_DIR=/assets/gitlab-shell
export CLOUD_GIT_GITLAB_SHELL_REPO_TAG=v5.1.1

export CLOUD_GIT_APK_RUNTIME_PACKAGES="\
    curl\
    git\
    redis \
    icu-libs \
    ruby \
    ruby-irb \
    ruby-io-console \
    openssh \
    nginx \
    nodejs \
    postgresql \
    tzdata \
    postfix \
    "
export CLOUD_GIT_APK_BUILD_TIME_PACKAGES="
    gettext \
    icu-dev \
    gcc \
    g++ \
    make \
    cmake \
    gettext \
    linux-headers \
    go \
    python2 \
    ruby-dev \
    musl-dev \
    krb5-dev \
    postgresql-dev \
    mysql-dev \
    sqlite-dev \
    zlib-dev \
    libffi-dev \
    postgresql-contrib \
    sqlite \
    krb5 \
    "
apk update && apk upgrade

#
# runtime packages
apk add --update ${CLOUD_GIT_APK_RUNTIME_PACKAGES}

#
# build time packages
apk add --update ${CLOUD_GIT_APK_BUILD_TIME_PACKAGES}
apk add yarn --update-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community

addgroup -g 998 git
adduser -u 998 -G git -s /bin/sh -h /var/opt/gitlab -D git

# global git settings
git config --global gc.auto 0
git config --global core.autocrlf input
git config --global repack.writeBitmaps true

#############################################

gem install bundler --no-ri --no-rdoc

git -c advice.detachedHead=false clone \
    -b ${CLOUD_GIT_GITLAB_REPO_TAG} \
    --depth 1 \
    ${CLOUD_GIT_GITLAB_REPO} \
    ${CLOUD_GIT_GITLAB_REPO_DIR}
rm -rf ${CLOUD_GIT_GITLAB_REPO_DIR}/.git

#
# disable gitaly
#
# @@ TODO -- push patch to upstream
# gitaly gem depends on grpc gem that seems to be incompatible with musl libc, remove it
cd ${CLOUD_GIT_GITLAB_REPO_DIR}
sed --in-place "s/^gem 'gitaly'.*/#&/" Gemfile
sed --in-place "s/^require 'gitaly'/#&/" lib/gitlab/gitaly_client.rb

# inject missing gem
bundle inject 'bigdecimal' '> 0'
(cd ${CLOUD_GIT_GITLAB_REPO_DIR} && RAILS_ENV=production bundle install --jobs 4 --no-deployment --path vendor/bundle --without development test)

(cd ${CLOUD_GIT_GITLAB_REPO_DIR} && RAILS_ENV=production bundle install --jobs 4 --without development test)

#
# gitlab shell -- called from ruby
#
git -c advice.detachedHead=false clone \
    -b ${CLOUD_GIT_GITLAB_SHELL_REPO_TAG} \
    --depth 1 \
    ${CLOUD_GIT_GITLAB_SHELL_REPO} \
    ${CLOUD_GIT_GITLAB_SHELL_REPO_DIR}
rm -rf ${CLOUD_GIT_GITLAB_SHELL_REPO_DIR}/.git

ln -s /assets/shell_config.yml ${CLOUD_GIT_GITLAB_SHELL_REPO_DIR}/config.yml

#
# disable gitaly
#
#sed --in-place "s/@gitaly = status.gitaly/@gitaly = false/" /assets/gitlab-shell/lib/gitlab_shell.rb
sed --in-place "s/GITALY_MIGRATED_COMMANDS = {/GITALY_MIGRATED_COMMANDS = {}; XX = {/" /assets/gitlab-shell/lib/gitlab_shell.rb

(cd ${CLOUD_GIT_GITLAB_SHELL_REPO_DIR} && RAILS_ENV=production bundle install --jobs 4)

#
# "runas" git user
#
chown -R git:git ${CLOUD_GIT_GITLAB_REPO_DIR}
chmod -R g+s,u+s ${CLOUD_GIT_GITLAB_REPO_DIR}/bin/*
chown -R git:git ${CLOUD_GIT_GITLAB_SHELL_REPO_DIR}
chmod -R g+s,u+s ${CLOUD_GIT_GITLAB_SHELL_REPO_DIR}/bin/*

#
# set up configuration files
#
envsubst < /assets/gitlab.template.yml > /assets/gitlab.yml
ln -s /assets/database.yml ${CLOUD_GIT_GITLAB_REPO_DIR}/config/database.yml
ln -s /assets/resque.yml ${CLOUD_GIT_GITLAB_REPO_DIR}/config/resque.yml
ln -s /assets/gitlab.yml ${CLOUD_GIT_GITLAB_REPO_DIR}/config/gitlab.yml
ln -s /var/opt/gitlab/gitlab-rails/etc/secrets.yml ${CLOUD_GIT_GITLAB_REPO_DIR}/config/secrets.yml
mkdir -p /etc/gitlab && cp /assets/etc_gitlab.rb /etc/gitlab/gitlab.rb

#
# set up executable dependencies
#
mkdir -p /opt/gitlab/embedded/bin
ln -s /usr/bin/git /opt/gitlab/embedded/bin/git
mkdir -p /opt/gitlab/embedded/service/gitlab-shell
ln -s /assets/gitlab-shell/bin /opt/gitlab/embedded/service/gitlab-shell/bin
ln -s /assets/gitlab-shell/hooks /opt/gitlab/embedded/service/gitlab-shell/hooks
ln -s /assets/gitlab-shell/VERSION /opt/gitlab/embedded/service/gitlab-shell/VERSION

#
# set up logging
#
mkdir -p /var/log/gitlab/gitlab-rails
mkdir -p /var/log/gitlab/gitlab-shell
chown -R git:git /var/log/gitlab
chmod -R +w /var/log/gitlab

apk del ${CLOUD_GIT_APK_BUILD_TIME_PACKAGES} yarn
rm -rf /var/cache/apk/*

exit 0
